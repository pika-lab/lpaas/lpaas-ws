package it.unibo.alice.lpaas.ws.routes;

import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;

import it.unibo.alice.lpaas.ws.persistence.GoalListStorage;
import it.unibo.alice.lpaas.ws.presentation.JsonIO;
import it.unibo.alice.lpaas.ws.presentation.json.CredentialJSON;
import it.unibo.alice.lpaas.ws.presentation.json.TokenJSON;
import it.unibo.alice.lpaas.ws.security.JwtSecurityManager;
import it.unibo.alice.lpaas.ws.security.Roles;

@Path("/auth")
@Stateless
public class AuthRoute {

    @EJB
    private JwtSecurityManager security;

    @EJB
    private GoalListStorage goalListStorage;

    @EJB
    private JsonIO json;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUserToken(final String credentialsString) {
        final CredentialJSON credentials = this.json.fromJson(credentialsString, CredentialJSON.class);

        try {
            final Roles role = this.security.validate(Objects.requireNonNull(credentials.getUsername()),
                    Objects.requireNonNull(credentials.getPassword()));
            if (role == null) {
                return Response.status(Status.UNAUTHORIZED).build();
            }

            final JwtClaims claims = this.security.getConfigurationClaims(credentials.getUsername(), role);
            final TokenJSON token = new TokenJSON(this.security.signClaimsAndGetSerialization(claims));

            return Response.ok().entity(this.json.toJson(token)).build();
        } catch (final NullPointerException e) {
            throw new BadRequestException(e);
        } catch (final JoseException e) {
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR, e);
        }
    }
}
