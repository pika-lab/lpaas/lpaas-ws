package it.unibo.alice.lpaas.ws.persistence.exceptions;

import it.unibo.alice.lpaas.ws.persistence.GoalListEntity;

public class GoalListAlreadyExistsException extends Exception {

    private static final long serialVersionUID = 8976751778564134440L;

    public GoalListAlreadyExistsException(final GoalListEntity goalList) {
        this(goalList.getName());
    }

    public GoalListAlreadyExistsException(final String name) {
        super(String.format("A goal list named '%s' already exists", name));
    }
}
