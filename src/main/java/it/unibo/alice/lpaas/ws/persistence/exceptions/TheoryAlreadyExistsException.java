package it.unibo.alice.lpaas.ws.persistence.exceptions;

import it.unibo.alice.lpaas.ws.persistence.TheoryEntity;

public class TheoryAlreadyExistsException extends Exception {
    private static final long serialVersionUID = 149051688478704723L;

    public TheoryAlreadyExistsException(final String name) {
        super(String.format("A theory named '%s' already exists", name));
    }

    public TheoryAlreadyExistsException(final TheoryEntity theory) {
        this(theory.getName());
    }
}