package it.unibo.alice.lpaas.ws.persistence.exceptions;

import it.unibo.alice.lpaas.ws.persistence.GoalListEntity;

public class NoSuchAGoalListException extends Exception {

    private static final long serialVersionUID = -4641494046867550959L;

    public NoSuchAGoalListException(final GoalListEntity goalList) {
        this(goalList.getName());
    }

    public NoSuchAGoalListException(final String name) {
        super(String.format("No such a goal list with name: '%s'", name));
    }
}
