package it.unibo.alice.lpaas.ws.utils;

import java.util.function.Function;

public interface ThrowingFunction<T, R, E extends Exception> {
    R apply(T t) throws E;

    default Function<T, R> toUnchecked() {
        return t -> {
            try {
                return this.apply(t);
            } catch (final Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}
