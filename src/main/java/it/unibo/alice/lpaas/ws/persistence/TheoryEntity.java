package it.unibo.alice.lpaas.ws.persistence;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.TheoryManager;
import it.unibo.alice.lpaas.ws.utils.Terms;

@Entity
@IdClass(NamedAndVersionedPrimaryKey.class)
@Table(name = "PROLOG_THEORIES")
public class TheoryEntity implements Serializable {

    private static final long serialVersionUID = -5066820215440166120L;

    @Id
    @Column(name = "name")
    private String name;

    @Id
    @Column(name = "version", nullable = false, updatable = false)
    private Long version;

    @Column(name = "theory_code", nullable = false)
    private String theoryCode;

    @Column(name = "timestamp", nullable = false)
    private Date timestamp;

    @Transient
    private transient Theory theory;

    @Transient
    private transient Prolog engine;

    public TheoryEntity() {
    }

    public TheoryEntity(final String name, final long version, final String theoryCode) {
        this.name = Objects.requireNonNull(name);
        this.theoryCode = Optional.of(theoryCode).orElse("");
        this.version = version;
    }

    public TheoryEntity(final String name, final String theoryCode) {
        this.name = Objects.requireNonNull(name);
        this.theoryCode = Optional.of(theoryCode).orElse("");
    }

    public TheoryEntity addFacts(final boolean beginning, final Stream<Term> facts) {
        final Prolog engine = getEngine();
        try {
            engine.setTheory(getTheory());
        } catch (final InvalidTheoryException e) {
            throw new IllegalStateException(e);
        }

        final TheoryManager tm = engine.getTheoryManager();
        final Consumer<Struct> assertOp = beginning ? s -> tm.assertA(s, true, null, true)
                : s -> tm.assertZ(s, true, null, true);

        facts.map(it -> (Struct) it).forEach(assertOp);

        setTheoryCode(tm.getTheory(true));

        return this;
    }

    public TheoryEntity addFacts(final boolean beginning, final Term... terms) {
        return addFacts(beginning, Arrays.stream(terms));
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof TheoryEntity)) {
            return false;
        }
        final TheoryEntity other = (TheoryEntity) obj;
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!this.version.equals(other.version)) {
            return false;
        }
        return true;
    }

    private Prolog getEngine() {
        if (this.engine == null) {
            this.engine = new Prolog();
        }
        return this.engine;
    }

    public Stream<Struct> getFacts(final String functor) throws InvalidTheoryException {
        return getPredicates().filter(p -> p.isStruct()).map(p -> (Struct) p).filter(s -> s.getName().equals(functor));
    }

    public Stream<Struct> getFacts(final String functor, final int arity) throws InvalidTheoryException {
        return getFacts(functor).filter(p -> p.getArity() == arity);
    }

    public String getName() {
        return this.name;
    }

    public Stream<Term> getPredicates() throws InvalidTheoryException {
        final Iterator<? extends Term> ir1 = getTheory().iterator(getEngine());
        final Iterator<Term> ir2 = new Iterator<Term>() {

            @Override
            public boolean hasNext() {
                return ir1.hasNext();
            }

            @Override
            public Term next() {
                return ir1.next();
            }
        };
        final Iterable<Term> ib = () -> ir2;
        return StreamSupport.stream(ib.spliterator(), false);
    }

    public Theory getTheory() throws InvalidTheoryException {
        if (this.theory == null) {
            this.theory = new Theory(getTheoryCode());
        }
        return this.theory;
    }

    public String getTheoryCode() {
        return this.theoryCode;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }

    public long getVersion() {
        return this.version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.name == null ? 0 : this.name.hashCode());
        result = prime * result + (this.version == null ? 0 : this.version.hashCode());
        return result;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setTheoryCode(final String theoryCode) {
        this.theoryCode = theoryCode;
        this.theory = null;
    }

    public void setTimestamp(final Date value) {
        this.timestamp = value;
    }

    public void setTimestampToNow() {
        this.timestamp = new Date();
    }

    public void setVersion(final long value) {
        this.version = value;
    }

    @Override
    public String toString() {
        return "PrologTheory [name=" + this.name + ", version=" + this.version + "]";
    }

    public TheoryEntity updateFacts(final boolean beginning, final Stream<Term> facts) {
        final Prolog engine = getEngine();
        try {
            engine.setTheory(getTheory());
        } catch (final InvalidTheoryException e) {
            throw new IllegalStateException(e);
        }

        final TheoryManager tm = engine.getTheoryManager();
        
        final Consumer<Struct> retractAndAssert = s -> {
            Struct template = Terms.template(s);
            try {
                SolveInfo si = engine.solve(new Struct(",",
                            new Struct("retract", template),
                            new Struct("asserta", s)
                        ));
                System.out.printf("retract(%s), assert(%s): %s", template, s, si.isSuccess() ? "success" : "fail");
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        facts.map(it -> (Struct) it).forEach(retractAndAssert);

        setTheoryCode(tm.getTheory(true));

        return this;
    }

    public TheoryEntity updateFacts(final boolean beginning, final Term... terms) {
        return updateFacts(beginning, Arrays.stream(terms));
    }
}