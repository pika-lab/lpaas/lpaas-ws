package it.unibo.alice.lpaas.ws.presentation.json;

public class CredentialJSON {
    private String username;
    private String password;

    public CredentialJSON() {
        super();
    }

    public CredentialJSON(final String username, final String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "CredentialJSON [getUsername()=" + getUsername() + ", getPassword()=" + getPassword() + "]";
    }

}
