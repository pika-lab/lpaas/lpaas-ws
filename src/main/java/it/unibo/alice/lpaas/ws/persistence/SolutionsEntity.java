package it.unibo.alice.lpaas.ws.persistence;

import java.io.Serializable;
import java.time.Duration;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;
import javax.persistence.Transient;

import alice.tuprolog.InvalidTermException;
import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.NoMoreSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;

@Entity
@IdClass(NamedAndVersionedPrimaryKey.class)
@Table(name = "SOLUTION_SET")
public class SolutionsEntity implements Serializable {

    public class Result {
        private final List<SolveInfo> results;
        private final boolean sideEffects;
        private final String newTheory;

        public Result(final List<SolveInfo> results, final boolean sideEffects, final String newTheory) {
            super();
            this.results = results;
            this.sideEffects = sideEffects;
            this.newTheory = newTheory;
        }

        public Optional<String> getNewTheory() {
            return Optional.ofNullable(this.newTheory);
        }

        public List<SolveInfo> getResults() {
            return this.results;
        }

        public boolean hasSideEffects() {
            return this.sideEffects;
        }

    }

    private static final long serialVersionUID = 5119895431111859285L;

    @Id
    @Column(name = "name")
    private String name;

    @Id
    @Column(name = "version", nullable = false, updatable = false)
    private Long version;

    @Column(name = "timestamp", nullable = false)
    private Date timestamp;

    @Column(name = "theory_name", nullable = false)
    private String theoryName;

    @Column(name = "theory_version", nullable = false)
    private Long theoryVersion;

    @ManyToOne
    @PrimaryKeyJoinColumns({ @PrimaryKeyJoinColumn(name = "theory_name", referencedColumnName = "name"),
            @PrimaryKeyJoinColumn(name = "theory_version", referencedColumnName = "version") })
    private TheoryEntity theory;

    @Column(name = "goal_list", nullable = false)
    private String goalListName;

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "goal_list", referencedColumnName = "name")
    private GoalListEntity goalList;

    @Column(name = "skip", nullable = true)
    private Long skip;

    @Column(name = "max_n_solution", nullable = true)
    private Long limit;

    @Column(name = "within", nullable = true)
    private Long within;

    @Transient
    private transient Prolog engine;

    public SolutionsEntity() {
    }

    public SolutionsEntity(final SolutionsEntity other) {
        this.name = other.name;
        this.version = other.version + 1;
        this.skip = other.skip;
        this.limit = other.limit;
        this.within = other.within;
        this.timestamp = new Date();
    }

    public SolutionsEntity(final String hook, final Long skip, final Long limit, final Duration within) {
        this.name = Optional.ofNullable(hook).orElseGet(() -> UUID.randomUUID().toString());
        this.version = 0L;
        this.skip = skip;
        this.limit = limit;
        this.within = within != null ? within.toMillis() : null;
        this.timestamp = new Date();
    }

    private Prolog getEngine() {
        if (this.engine == null) {
            this.engine = new Prolog();
        }
        return this.engine;
    }

    public GoalListEntity getGoalList() {
        return this.goalList;
    }

    public String getHook() {
        return this.name;
    }

    public long getLimit() {
        return this.limit == null ? Long.MAX_VALUE : this.limit.longValue();
    }

    public TheoryEntity getPrologTheory() {
        return this.theory;
    }

    public Result getResult() throws InvalidTheoryException, InvalidTermException {
        final Theory theory = getPrologTheory().getTheory();
        final Term goal = getGoalList().getGoalListTerm();
        final Prolog engine = getEngine();
        final List<SolveInfo> result = new LinkedList<>();
        boolean pure = true;

        engine.setTheory(theory);

        String kbPrev = engine.getTheoryManager().getTheory(true);
        String kb = null;
        SolveInfo si = engine.solve(goal);

        for (long i = 0; si.isSuccess() && i < getSkip(); i++) {
            try {
                kb = engine.getTheoryManager().getTheory(true);
                si = engine.solveNext();
            } catch (final NoMoreSolutionException e) {
                break;
            } finally {
                pure = pure && kbPrev.equals(kb);
                kbPrev = kb;
            }
        }

        result.add(si);

        for (long i = 1; si.isSuccess() && i < getLimit(); i++) {
            try {
                kb = engine.getTheoryManager().getTheory(true);
                si = engine.solveNext();
                result.add(si);
            } catch (final NoMoreSolutionException e) {
                break;
            } finally {
                pure = pure && kbPrev.equals(kb);
                kbPrev = kb;
            }
        }

        kb = engine.getTheoryManager().getTheory(true);
        pure = pure && kbPrev.equals(kb);

        return new Result(result, !pure, pure ? null : kb);
    }

    public long getSkip() {
        return this.skip == null ? 0 : this.skip.longValue();
    }

    public Date getTimestamp() {
        return this.timestamp;
    }

    public long getVersion() {
        return this.version.longValue();
    }

    public long getWithin() {
        return this.within == null ? Long.MAX_VALUE : this.within.longValue();
    }

    public void setGoalList(final GoalListEntity goalList) {
        this.goalList = goalList;
        if (goalList != null) {
            this.goalListName = goalList.getName();
        }
    }

    public void setHook(final String value) {
        this.name = value;
    }

    public void setLimit(final long limit) {
        this.limit = limit < 1 ? 1 : limit;
    }

    public void setSkip(final long skip) {
        this.skip = skip < 0 ? 0 : skip;
    }

    public void setTheory(final TheoryEntity theory) {
        this.theory = theory;
        if (theory != null) {
            this.theoryName = theory.getName();
            this.theoryVersion = theory.getVersion();
        }
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setTimestampToNow() {
        setTimestamp(new Date());
    }

    public void setVersion(final long version) {
        this.version = version;
    }

    public void setWithin(final Duration duration) {
        final long millis = duration.toMillis();
        this.within = millis < 1000 ? 1000 : millis;
    }

}
