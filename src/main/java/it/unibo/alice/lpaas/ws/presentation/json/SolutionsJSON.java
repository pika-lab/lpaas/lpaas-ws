package it.unibo.alice.lpaas.ws.presentation.json;

import java.util.List;
import java.util.stream.Collectors;

import alice.tuprolog.InvalidTermException;
import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.SolveInfo;
import it.unibo.alice.lpaas.ws.persistence.SolutionsEntity;

public class SolutionsJSON {
    private String hook;
    private long version;
    private String theory;
    private String goalList;
    private SolutionJSON[] solutions;
    private String timestamp;

    public SolutionsJSON() {
    }

    public SolutionsJSON(final SolutionsEntity ss) {
        this.hook = String.format("/solutions/%s", ss.getHook());
        this.version = ss.getVersion();
        this.theory = String.format("/theories/%s/history/%s", ss.getPrologTheory().getName(),
                ss.getPrologTheory().getVersion());
        this.goalList = String.format("/goals/%s", ss.getGoalList().getName());
        this.timestamp = ss.getTimestamp().toGMTString();

    }

    public SolutionsJSON includeSolutions(final List<SolveInfo> ss)
            throws InvalidTermException, InvalidTheoryException {
        this.solutions = ss.stream().map(SolutionJSON::new).collect(Collectors.toList()).toArray(new SolutionJSON[0]);

        return this;
    }
}
