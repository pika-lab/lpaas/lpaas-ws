package it.unibo.alice.lpaas.ws.persistence.exceptions;

import it.unibo.alice.lpaas.ws.persistence.SolutionsEntity;

public class SolutionsAlreadyExistException extends Exception {

    private static final long serialVersionUID = -971560684122676915L;

    public SolutionsAlreadyExistException(final SolutionsEntity solutionSet) {
        this(solutionSet.getHook());
    }

    public SolutionsAlreadyExistException(final String hook) {
        super(String.format("The hook '%s' is already to another solution set", hook));
    }
}
