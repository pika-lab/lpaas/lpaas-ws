package it.unibo.alice.lpaas.ws.routes;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import alice.tuprolog.InvalidTermException;
import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.alice.lpaas.ws.core.AuthCheckInterceptor;
import it.unibo.alice.lpaas.ws.core.RequiresRole;
import it.unibo.alice.lpaas.ws.persistence.TheoryEntity;
import it.unibo.alice.lpaas.ws.persistence.TheoryStorage;
import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchATheoryException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.TheoryAlreadyExistsException;
import it.unibo.alice.lpaas.ws.security.JwtSecurityManager;
import it.unibo.alice.lpaas.ws.security.Roles;

/**
 * This is the component that implements the public part of the server's user
 * interface. As such, it contains methods to access some of the the
 * configurations of the server and, more important, methods to request the
 * resolution (with or without session state support) of a goal from the
 * goalList.</br>
 * The methods don't require any authentication, and therefore no login method
 * is provided.</br>
 * All the public methods are exposed to the client as RESTful web services.
 *
 * @author Andrea Muccioli
 *
 */
@Path("/theories")
@Stateless
@Interceptors(AuthCheckInterceptor.class)
public class TheoriesRoute {

    @EJB
    private JwtSecurityManager security;

    @EJB
    private TheoryStorage theoryStorage;

    @POST
    @Path("/{name}/facts/")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @RequiresRole(Roles.SENSOR)
    public Response addNewFactToTheory(@PathParam("name") final String name,
            @QueryParam("beginning") final Boolean beginning, final String fact,
            final @HeaderParam("Authorization") String auth) {
        try {
            final TheoryEntity te = this.theoryStorage.getLastVersion(name);
            final boolean insertOnTop = beginning != null && beginning.booleanValue();
            final Struct term = (Struct) Term.parse(fact);
            final TheoryEntity newTe = this.theoryStorage.addVersion(te.addFacts(insertOnTop, term));

            return Response.ok().entity(String.format("/theories/%s/facts/%s/history/%s", newTe.getName(),
                    term.getName(), newTe.getVersion())).build();
        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        } catch (final InvalidTermException | ClassCastException e1) {
            throw new BadRequestException(e1);
        }
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes("text/plain")
    @RequiresRole(Roles.CONFIGURATOR)
    public Response addNewTheory(final String source, @QueryParam("name") final String name,
            final @HeaderParam("Authorization") String auth) throws ClientErrorException {
        TheoryEntity theory;
        try {
            theory = this.theoryStorage.addTheory(name, source);
        } catch (final TheoryAlreadyExistsException e) {
            throw new ClientErrorException(e.getMessage(), Status.CONFLICT);
        }
        return Response.ok().entity(String.format("/theories/%s/history/%s", theory.getName(), theory.getVersion()))
                .build();
    }

    @POST
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes("text/plain")
    @RequiresRole(Roles.CONFIGURATOR)
    public Response addNewTheoryVersion(final String source, @PathParam("name") final String name,
            final @HeaderParam("Authorization") String auth) throws ClientErrorException {
        final TheoryEntity theory = this.theoryStorage.addVersion(name, source);

        return Response.ok().entity(String.format("/theories/%s/history/%s", theory.getName(), theory.getVersion()))
                .build();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getTheories() {
        return this.theoryStorage.getAllLastVersions()
                .map(pt -> String.format("/theories/%s/history/%s", pt.getName(), pt.getVersion()))
                .collect(Collectors.joining("\n", "Available theories:\n", ""));
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getTheory(@PathParam("name") final String name) {
        try {
            return this.theoryStorage.getLastVersion(name).getTheoryCode();
        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        }
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTheoryAsJSON(@PathParam("name") final String name) {
        try {
            return this.theoryStorage.getLastVersion(name).getTheory().toJSON();
        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        } catch (final InvalidTheoryException e) {
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR, e);
        }
    }

    @GET
    @Path("/{name}/facts/{functor}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getTheoryFacts(@PathParam("name") final String name, @PathParam("functor") final String functor,
            @QueryParam("arity") final Integer arity, @QueryParam("limit") final Long limit) {
        try {
            final TheoryEntity te = this.theoryStorage.getLastVersion(name);
            return getTheoryFactsImpl(te, functor, arity, limit);

        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        }
    }

    private String getTheoryFactsImpl(final TheoryEntity theory, final String functor, final Integer arity,
            final Long limit) {
        try {
            final Stream<Struct> ss = arity == null ? theory.getFacts(functor) : theory.getFacts(functor, arity);

            return ss.limit(Optional.ofNullable(limit).orElse(Long.MAX_VALUE)).map(Object::toString)
                    .collect(Collectors.joining(".\n\n"));

        } catch (final InvalidTheoryException e) {
            throw new InternalError(e);
        }
    }

    @GET
    @Path("/{name}/facts/{functor}/history/{version}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getTheoryFactsVersion(@PathParam("name") final String name,
            @PathParam("version") final String version, @PathParam("functor") final String functor,
            @QueryParam("arity") final Integer arity, @QueryParam("limit") final Long limit) {
        try {
            final TheoryEntity te = this.theoryStorage.getVersion(name, Long.parseLong(version));
            return getTheoryFactsImpl(te, functor, arity, limit);
        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        }
    }

    @GET
    @Path("/{name}/history/{version}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getTheoryVersion(@PathParam("name") final String name, @PathParam("version") final String version) {
        try {
            return this.theoryStorage.getVersion(name, Long.parseLong(version)).getTheoryCode();
        } catch (final NumberFormatException e) {
            throw new ServerErrorException(Status.NOT_IMPLEMENTED, e);
        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        }
    }

    @DELETE
    @Path("/{name}")
    @RequiresRole(Roles.CONFIGURATOR)
    public Response removeTheory(@PathParam("name") final String name, final @HeaderParam("Authorization") String auth)
            throws ClientErrorException {
        try {
            this.theoryStorage.removeTheory(name);
        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        }

        return Response.status(Status.NO_CONTENT).build();
    }

    @PUT
    @Path("/{name}/facts/{functor}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @RequiresRole(Roles.SENSOR)
    public Response updateFactInTheory(@PathParam("name") final String name, @PathParam("functor") final String functor,
            @QueryParam("beginning") final Boolean beginning, final String fact,
            final @HeaderParam("Authorization") String auth) {
        try {
            final TheoryEntity te = this.theoryStorage.getLastVersion(name);
            final boolean insertOnTop = beginning != null && beginning.booleanValue();
            final Struct term = (Struct) Term.parse(fact);

            if (!term.getName().equals(functor)) {
                throw new BadRequestException();
            }

            final TheoryEntity newTe = this.theoryStorage.addVersion(te.updateFacts(insertOnTop, term));

            return Response.ok().entity(String.format("/theories/%s/facts/%s/history/%s", newTe.getName(),
                    term.getName(), newTe.getVersion())).build();
        } catch (final NoSuchATheoryException e) {
            throw new NotFoundException(e);
        } catch (final InvalidTermException | ClassCastException e1) {
            throw new BadRequestException(e1);
        }
    }

}
