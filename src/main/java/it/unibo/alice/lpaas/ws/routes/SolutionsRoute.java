package it.unibo.alice.lpaas.ws.routes;

import java.time.Duration;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.JsonSyntaxException;

import alice.tuprolog.InvalidTermException;
import alice.tuprolog.InvalidTheoryException;
import it.unibo.alice.lpaas.ws.persistence.GoalListEntity;
import it.unibo.alice.lpaas.ws.persistence.GoalListStorage;
import it.unibo.alice.lpaas.ws.persistence.SolutionsEntity;
import it.unibo.alice.lpaas.ws.persistence.SolutionsStorage;
import it.unibo.alice.lpaas.ws.persistence.TheoryEntity;
import it.unibo.alice.lpaas.ws.persistence.TheoryStorage;
import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchAGoalListException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchATheoryException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchSolutionsException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.SolutionsAlreadyExistException;
import it.unibo.alice.lpaas.ws.presentation.JsonIO;
import it.unibo.alice.lpaas.ws.presentation.json.GoalsAndTheoryJSON;
import it.unibo.alice.lpaas.ws.presentation.json.SolutionsJSON;
import it.unibo.alice.lpaas.ws.security.JwtSecurityManager;

@Path("/solutions")
@Stateless
public class SolutionsRoute {

    private static Pattern GOAL_URL = Pattern.compile("^.*?\\/goals\\/(?<name>\\w+)\\/?$");

    private static Pattern THEORIES_URL = Pattern
            .compile("^.*?\\/theories\\/(?<name>\\w+)(?:\\/history\\/(?<version>.+?))?\\/?$");

    @EJB
    private JwtSecurityManager security;

    @EJB
    private SolutionsStorage solutionsStorage;

    @EJB
    private GoalListStorage goalListStorage;

    @EJB
    private TheoryStorage theoriesStorage;

    @EJB
    private JsonIO json;

    @Resource
    private TimerService timer;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String addNewSolutionSet(final String goalsAndTheory, @QueryParam("hook") final String hook,
            @QueryParam("skip") final Long skip, @QueryParam("limit") final Long limit,
            @QueryParam("within") final String within, @QueryParam("every") final String every) {

        SolutionsEntity solutionEntity;
        GoalsAndTheoryJSON goalTheory;

        try {
            solutionEntity = new SolutionsEntity(hook, skip, limit, Optional.ofNullable(within).map(Duration::parse).orElse(Duration.ofMinutes(1)));
            goalTheory = this.json.fromJson(Optional.ofNullable(goalsAndTheory).orElse("{}"), GoalsAndTheoryJSON.class);

            final TheoryEntity pt = getTheoryFromInternalURL(goalTheory.getTheory());
            final Duration everyDuration = every == null ? null : Duration.parse(every);

            solutionEntity.setGoalList(getGoalFromInternalURL(goalTheory.getGoals()));

            solutionEntity.setTheory(pt);

            this.solutionsStorage.addSolutionSet(solutionEntity);

            final SolutionsEntity.Result results = solutionEntity.getResult();

            if (everyDuration != null) {
                this.timer.createIntervalTimer(everyDuration.toMillis(), everyDuration.toMillis(),
                        new TimerConfig(solutionEntity, true));
            }

            if (results.hasSideEffects()) {
                this.theoriesStorage.addVersion(pt.getName(), results.getNewTheory().get());
            }

            return this.json.toJson(new SolutionsJSON(solutionEntity).includeSolutions(results.getResults()));

        } catch (final SolutionsAlreadyExistException e) {
            throw new ClientErrorException(e.getMessage(), Status.CONFLICT);
        } catch (final InvalidTermException | DateTimeParseException | JsonSyntaxException e1) {
            throw new BadRequestException(e1);
        } catch (final IllegalArgumentException e2) {
            throw new BadRequestException(e2);
        } catch (final NoSuchAGoalListException | NoSuchATheoryException e3) {
            throw new NotFoundException(e3);
        } catch (final InvalidTheoryException e) {
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR, e);
        }
    }

    @Timeout
    public void doTask(final Timer timer) {
        try {
            if (!(timer.getInfo() instanceof SolutionsEntity)) {
                throw new IllegalStateException();
            }

            final SolutionsEntity oldSolution = (SolutionsEntity) timer.getInfo();
            SolutionsEntity newSolution = new SolutionsEntity(oldSolution);

            if (!this.solutionsStorage.isHookPresent(oldSolution.getHook())) {
                throw new IllegalStateException();
            }

            newSolution.setTheory(this.theoriesStorage.getLastVersion(oldSolution.getPrologTheory().getName()));
            newSolution.setGoalList(this.goalListStorage.getGoalList(oldSolution.getGoalList().getName()));

            newSolution = this.solutionsStorage.addVersion(newSolution);
            System.out.printf("Published solution /solutions/%s/history/%d\n", newSolution.getHook(),
                    newSolution.getVersion());

            final SolutionsEntity.Result results = newSolution.getResult();
            if (results.hasSideEffects()) {
                final TheoryEntity te = this.theoriesStorage.addVersion(newSolution.getPrologTheory().getName(),
                        results.getNewTheory().get());
                System.out.printf("Published theory /theories/%s/history/%d\n", te.getName(), te.getVersion());
            }
        } catch (NoSuchATheoryException | NoSuchAGoalListException | InvalidTermException | InvalidTheoryException
                | IllegalStateException | NullPointerException e) {
            timer.cancel();
        }
    }

    private GoalListEntity getGoalFromInternalURL(final String url) throws NoSuchAGoalListException {
        final Matcher m = GOAL_URL.matcher(url);

        if (!m.matches()) {
            throw new IllegalArgumentException(url);
        }

        final String name = m.group("name");

        return this.goalListStorage.getGoalList(name);
    }

    @GET
    @Path("/{hook}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getHook(@PathParam("hook") final String hook) throws IllegalStateException {
        if (hook == null || hook.isEmpty()) {
            throw new BadRequestException();
        }
        try {
            final SolutionsEntity solution = this.solutionsStorage.getLastVersion(hook);
            final SolutionsEntity.Result result = solution.getResult();
            return this.json.toJson(new SolutionsJSON(solution).includeSolutions(result.getResults()));

        } catch (final NoSuchSolutionsException e) {
            throw new NotFoundException(e);
        } catch (final InvalidTermException e) {
            throw new IllegalStateException(
                    "The referenced goal list changed with respect to the last solution request");
        } catch (final InvalidTheoryException e) {
            throw new IllegalStateException("The referenced theory changed with respect to the last solution request");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getHooks() {
        return this.solutionsStorage.getAllLastVersions()
                .map(it.unibo.alice.lpaas.ws.presentation.json.SolutionsJSON::new).map(this.json::toJson)
                .collect(Collectors.joining(", ", "[ ", " ]"));
    }

    @GET
    @Path("/{hook}/history/{version}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getHookVersion(@PathParam("hook") final String hook, @PathParam("version") final String version)
            throws IllegalStateException {
        if (hook == null || hook.isEmpty()) {
            throw new BadRequestException();
        }
        try {
            final SolutionsEntity solution = this.solutionsStorage.getVersion(hook, Long.parseLong(version));
            final SolutionsEntity.Result result = solution.getResult();
            return this.json.toJson(new SolutionsJSON(solution).includeSolutions(result.getResults()));
        } catch (final NumberFormatException e) {
            throw new ServerErrorException(Status.NOT_IMPLEMENTED, e);
        } catch (final NoSuchSolutionsException e) {
            throw new NotFoundException(e);
        } catch (final InvalidTermException e) {
            throw new IllegalStateException(
                    "The referenced goal list changed with respect to the last solution request");
        } catch (final InvalidTheoryException e) {
            throw new IllegalStateException("The referenced theory changed with respect to the last solution request");
        }
    }

    private TheoryEntity getTheoryFromInternalURL(final String url) throws NoSuchATheoryException {
        final Matcher m = THEORIES_URL.matcher(url);

        if (!m.matches()) {
            throw new IllegalArgumentException(url);
        }

        final String name = m.group("name");
        final Optional<Long> version = Optional.ofNullable(m.group("version")).map(x -> Long.parseLong(x));

        if (version.isPresent()) {
            return this.theoriesStorage.getVersion(name, version.get().longValue());
        } else {
            return this.theoriesStorage.getLastVersion(name);
        }
    }

    @DELETE
    public Response removeAll() {
        this.solutionsStorage.removeAll();

        return Response.status(Status.NO_CONTENT).build();
    }

    @DELETE
    @Path("/{hook}")
    public Response removeHook(@PathParam("hook") final String hook) {
        if (hook == null || hook.isEmpty()) {
            throw new BadRequestException();
        }
        try {
            this.solutionsStorage.removeSolution(hook);
            return Response.status(Status.NO_CONTENT).build();
        } catch (final NoSuchSolutionsException e) {
            throw new NotFoundException(e);
        }
    }

    @DELETE
    @Path("/{hook}/history/{version}")
    public Response removeHookVersion(@PathParam("hook") final String hook,
            @PathParam("version") final String version) {
        if (hook == null || hook.isEmpty()) {
            throw new BadRequestException();
        }
        try {
            this.solutionsStorage.removeVersion(hook, Long.parseLong(version));
            return Response.status(Status.NO_CONTENT).build();
        } catch (final NoSuchSolutionsException e) {
            throw new NotFoundException(e);
        } catch (final NumberFormatException e) {
            throw new BadRequestException();
        }
    }
}
