package it.unibo.alice.lpaas.ws.presentation;

import java.io.Reader;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Stateless;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

@Stateless
@LocalBean
@Singleton
public class JsonIO {

    private final Gson gson = new Gson();

    public <T> T fromJson(final JsonElement json, final Class<T> classOfT) throws JsonSyntaxException {
        return this.gson.fromJson(json, classOfT);
    }

    public <T> T fromJson(final Reader json, final Class<T> classOfT) throws JsonSyntaxException, JsonIOException {
        return this.gson.fromJson(json, classOfT);
    }

    public <T> T fromJson(final String json, final Class<T> classOfT) throws JsonSyntaxException {
        return this.gson.fromJson(json, classOfT);
    }

    public String toJson(final JsonElement jsonElement) {
        return this.gson.toJson(jsonElement);
    }

    public String toJson(final Object src) {
        return this.gson.toJson(src);

    }

}
