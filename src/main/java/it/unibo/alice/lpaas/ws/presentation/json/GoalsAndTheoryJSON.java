package it.unibo.alice.lpaas.ws.presentation.json;

import java.util.Optional;

public class GoalsAndTheoryJSON {

    private String goals;
    private String theory;

    public GoalsAndTheoryJSON() {
    }

    public String getGoals() {
        return Optional.ofNullable(this.goals).orElse("/goals/default");
    }

    public String getTheory() {
        return Optional.ofNullable(this.theory).orElse("/theories/default");
    }

    public void setGoals(final String goals) {
        this.goals = goals;
    }

    public void setTheory(final String theory) {
        this.theory = theory;
    }

    @Override
    public String toString() {
        return "GoalsAndTheory [getGoals()=" + getGoals() + ", getTheory()=" + getTheory() + "]";
    }

}
