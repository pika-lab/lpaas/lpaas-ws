package it.unibo.alice.lpaas.ws.presentation.json;

import alice.tuprolog.NoSolutionException;
import alice.tuprolog.SolveInfo;

public class SolutionJSON {
    private boolean success;
    private String solution;

    public SolutionJSON() {

    }

    public SolutionJSON(final SolveInfo si) {
        if (this.success = si.isSuccess()) {
            try {
                this.solution = si.getSolution().toString();
            } catch (final NoSolutionException e) {
                this.solution = null;
            }
        }
    }

    @Override
    public String toString() {
        return "Solution [success=" + this.success + ", solution=" + this.solution + "]";
    }

}
