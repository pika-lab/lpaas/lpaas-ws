package it.unibo.alice.lpaas.ws.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

//import it.unibo.alice.lpaas.ws.persistence.ConfigurationStorage;
import it.unibo.alice.lpaas.ws.persistence.GoalListStorage;
import it.unibo.alice.lpaas.ws.persistence.TheoryStorage;
import it.unibo.alice.lpaas.ws.persistence.UserEntity;
import it.unibo.alice.lpaas.ws.persistence.UserStorage;
import it.unibo.alice.lpaas.ws.persistence.exceptions.GoalListAlreadyExistsException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.TheoryAlreadyExistsException;
import it.unibo.alice.lpaas.ws.security.Roles;

/**
 * This component initializes the server, it executes at startup and is used to
 * load configurations from the deployment archive, setup users, etc...
 *
 * @author Andrea Muccioli
 *
 */
@Startup
@Singleton
@LocalBean
public class StartupOperations {

    @EJB
    private UserStorage users;

//    @EJB
//    private ConfigurationStorage plConfs;

    @EJB
    private TheoryStorage theoryStorage;

    @EJB
    private GoalListStorage goalListStorage;

    public StartupOperations() {
    }

    private void ensureDefaultGoalListExists() {
        if (!this.goalListStorage.isGoalListPresent("default")) {
            try {
                this.goalListStorage.addGoalList("default", "is_default");
            } catch (final GoalListAlreadyExistsException e) {
                throw new IllegalStateException();
            }
        }
    }

    private void ensureDefaultTheoryExists() {
        if (!this.theoryStorage.isTheoryPresent("default")) {
            try {
                this.theoryStorage.addTheory("default", "is_default.");
            } catch (final TheoryAlreadyExistsException e) {
                throw new IllegalStateException();
            }
        }
    }

    /**
     * Executes startup operations for the server application.
     */
    @PostConstruct
    private void initialize() {
        // if the user database is empty, load users from configuration xml file
//        if (this.users.getAllUsers().size() == 0) {
            final List<UserEntity> defaultUsers = readUserXMLConfiguration();
            System.out.println("Utenti letti: " + defaultUsers.toString());
            defaultUsers.forEach(u -> {
                if (users.getUser(u.getUsername()) != null) {
                    users.updateUser(u);
                } else {
                    users.addUser(u);
                }
            }); 
            
        // if no admin is set, add a default one, if default admin username is already
//        }
        // taken, promote an existing user
        if (this.users.getUsersWithRole(Roles.ADMIN).size() <= 0) {
            final UserEntity admin = UserEntity.getDefaultAdmin();
            if (this.users.getUserRole(admin.getUsername()) != null) {
                Roles r = Roles.ADMIN;
                boolean promoted = false;
                while ((r = r.getRoleBelow()) != null && !promoted) {
                    final List<UserEntity> list = this.users.getUsersWithRole(r);
                    if (list.size() > 0) {
                        list.get(0).setRole(Roles.ADMIN);
                        promoted = true;
                        System.out.println("Promuovo ad admin di default: " + list.get(0));
                    }
                }
            } else {
                System.out.println("Imposto admin di default: " + admin);
                this.users.addUser(admin);
            }
        }
//        // if no configuration is set, add a default empty one
//        if (this.plConfs.getConfiguration() == null) {
//            this.plConfs.resetConfiguration();
//        }

        ensureDefaultTheoryExists();
        ensureDefaultGoalListExists();
    }

    /**
     * Reads user credentials from the XML configuration file
     * "user_configuration.xml", contained in the deployment archive, and creates a
     * list of User based on the information read.
     *
     * @return A List of User containing the user information read from the
     *         configuration file.
     */
    private List<UserEntity> readUserXMLConfiguration() {
        final SAXBuilder builder = new SAXBuilder();
        final InputStream stream = this.getClass().getClassLoader().getResourceAsStream("user_configuration.xml");
        Document doc;
        try {
            doc = builder.build(stream);
        } catch (final JDOMException e) {
            e.printStackTrace();
            return null;
        } catch (final IOException e) {
            e.printStackTrace();
            return null;
        }

        final List<UserEntity> toReturn = new ArrayList<UserEntity>();
        final List<Element> users = doc.getRootElement().getChildren("user");
        users.forEach(el -> {
            final String username = el.getChildText("username");
            final String password = el.getChildText("password");
            final String roleString = el.getChildText("role");
            final Roles role = Roles.valueOf(roleString.toUpperCase());
            toReturn.add(new UserEntity(username, password, role));
        });

        return toReturn;
    }

}
