package it.unibo.alice.lpaas.ws.routes;

import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import alice.tuprolog.InvalidTermException;
import it.unibo.alice.lpaas.ws.core.AuthCheckInterceptor;
import it.unibo.alice.lpaas.ws.core.RequiresRole;
import it.unibo.alice.lpaas.ws.persistence.GoalListEntity;
import it.unibo.alice.lpaas.ws.persistence.GoalListStorage;
import it.unibo.alice.lpaas.ws.persistence.exceptions.GoalListAlreadyExistsException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchAGoalListException;
import it.unibo.alice.lpaas.ws.security.JwtSecurityManager;
import it.unibo.alice.lpaas.ws.security.Roles;

@Path("/goals")
@Stateless
@Interceptors(AuthCheckInterceptor.class)
public class GoalsRoute {

    @EJB
    private JwtSecurityManager security;

    @EJB
    private GoalListStorage goalListStorage;

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes("text/plain")
    @RequiresRole(Roles.CONFIGURATOR)
    public Response addNewGoalList(final String goals, @QueryParam("name") final String name,
            final @HeaderParam("Authorization") String auth) throws ClientErrorException {
        GoalListEntity gl;
        try {
            gl = this.goalListStorage.addGoalList(name, goals);
        } catch (final GoalListAlreadyExistsException e) {
            throw new ClientErrorException(e.getMessage(), Status.CONFLICT);
        } catch (final InvalidTermException e1) {
            throw new BadRequestException(e1);
        }
        return Response.ok().entity(String.format("/goals/%s", gl.getName())).build();
    }

    @POST
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes("text/plain")
    @RequiresRole(Roles.CONFIGURATOR)
    public Response addNewSubGoal(final String source, @PathParam("name") final String name,
            final @HeaderParam("Authorization") String auth) throws ClientErrorException {
        GoalListEntity gl;
        try {
            gl = this.goalListStorage.appendSubGoal(name, source);
        } catch (final NoSuchAGoalListException e) {
            throw new NotFoundException(e);
        }

        return Response.ok().entity(String.format("/goals/%s/%d", gl.getName(), gl.getLength())).build();
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getGoalList(@PathParam("name") final String name) {
        try {
            return this.goalListStorage.getGoalList(name).getGoalList();
        } catch (final NoSuchAGoalListException e) {
            throw new NotFoundException(e);
        }
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getGoalLists() {
        return this.goalListStorage.getAll().map(gl -> String.format("/goals/%s", gl.getName()))
                .collect(Collectors.joining("\n", "Available goals:\n", ""));
    }

    @GET
    @Path("/{name}/{index}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getSubGoal(@PathParam("name") final String name, @PathParam("index") final int index) {
        try {
            return this.goalListStorage.getGoalList(name).getSubGoal(index).toString();
        } catch (final NoSuchAGoalListException e) {
            throw new NotFoundException(e);
        }
    }

    @DELETE
    @Path("/{name}")
    @RequiresRole(Roles.CONFIGURATOR)
    public Response removeGoalList(@PathParam("name") final String name) throws ClientErrorException {
        try {
            this.goalListStorage.removeGoalList(name);
        } catch (final NoSuchAGoalListException e) {
            throw new NotFoundException(e);
        }

        return Response.status(Status.NO_CONTENT).build();
    }

    @PUT
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.TEXT_PLAIN)
    @RequiresRole(Roles.CONFIGURATOR)
    public Response replaceGoalList(final String source, @PathParam("name") final String name,
            final @HeaderParam("Authorization") String auth) {
        GoalListEntity gl;
        try {
            gl = this.goalListStorage.getGoalList(name);
            gl.setGoalListTerm(source);
            this.goalListStorage.updateGoalList(gl);
        } catch (final NoSuchAGoalListException e) {
            throw new NotFoundException(e);
        } catch (final InvalidTermException e) {
            throw new BadRequestException(e);
        }

        return Response.noContent().build();
    }
    
    @DELETE
    @Path("/{name}")
    @RequiresRole(Roles.CONFIGURATOR)
    public Response removeGoalList(final String source, @PathParam("name") final String name,
            final @HeaderParam("Authorization") String auth) {
        GoalListEntity gl;
        try {
            gl = this.goalListStorage.getGoalList(name);
            if ("default".equals(name)) {
                gl.setGoalListTerm("fail"); 
                this.goalListStorage.updateGoalList(gl);
            } else {
                this.goalListStorage.removeGoalList(gl.getName());
            }
        } catch (final NoSuchAGoalListException e) {
            throw new NotFoundException(e);
        } catch (final InvalidTermException e) {
            throw new BadRequestException(e);
        }

        return Response.noContent().build();
    }
}
