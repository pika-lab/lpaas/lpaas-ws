package it.unibo.alice.lpaas.ws.presentation.json;

public class TokenJSON {
    private String token;

    public TokenJSON() {
        super();
    }

    public TokenJSON(final String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Token [token=" + this.token + "]";
    }

}
