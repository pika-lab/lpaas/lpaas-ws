package it.unibo.alice.lpaas.ws.persistence.exceptions;

import it.unibo.alice.lpaas.ws.persistence.TheoryEntity;

public class NoSuchATheoryException extends Exception {
    private static final long serialVersionUID = 149051688478704723L;

    public NoSuchATheoryException(final String name) {
        super(String.format("No such a theory name: '%s'", name));
    }

    public NoSuchATheoryException(final String name, final long version) {
        super(String.format("No such a theory with name '%s' and version %d", name, version));
    }

    public NoSuchATheoryException(final TheoryEntity theory) {
        this(theory.getName());
    }
}