package it.unibo.alice.lpaas.ws.utils;

import java.util.LinkedList;
import java.util.List;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;

public class Terms {

    public static Term conjunctList(final List<Term> list) {
        final int size = list.size();
        if (size == 0) {
            return null;
        } else if (size == 1) {
            return list.get(0);
        } else {
            Struct couple = new Struct(",", list.get(size - 2), list.get(size - 1));
            for (int i = size - 3; i >= 0; i--) {
                couple = new Struct(",", list.get(i), couple);
            }
            return couple;
        }
    }

    public static List<Term> flattenConjunction(final Term term) {
        final List<Term> list = new LinkedList<>();

        if (term == null) {
            return list;
        }

        Term curr = term;
        Struct currStruct;
        while (curr.isStruct() && (currStruct = (Struct) curr).getName().equals(",") && currStruct.getArity() == 2) {
            list.add(currStruct.getArg(0));
            curr = currStruct.getArg(1);
        }
        list.add(curr);

        return list;
    }

    public static Struct template(final Struct term) {
        final Var[] vars = new Var[term.getArity()];
        for (int i = 0; i < vars.length; i++) {
            vars[i] = new Var();
        }
        return new Struct(term.getName(), vars);
    }

}
