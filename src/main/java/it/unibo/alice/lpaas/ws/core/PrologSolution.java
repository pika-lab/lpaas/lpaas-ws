package it.unibo.alice.lpaas.ws.core;

import java.util.ArrayList;
import java.util.List;

import alice.tuprolog.SolveInfo;

/**
 * This is a container object to encapsulate both a List of SolveInfo from the
 * solution of a goal on a Prolog engine and the serialized state at the end of
 * the above-mentioned resolution.
 *
 * @author Andrea Muccioli
 *
 */
public class PrologSolution {

    private List<SolveInfo> info;
    private String engineState;

    public PrologSolution(final List<SolveInfo> info, final String state) {
        this.info = info;
        this.engineState = state;
    }

    public PrologSolution(final SolveInfo info, final String state) {
        this.info = new ArrayList<SolveInfo>();
        this.info.add(info);
        this.engineState = state;
    }

    public void addInfo(final SolveInfo info) {
        this.info.add(info);
    }

    public String getEngineState() {
        return this.engineState;
    }

    public SolveInfo getFirstInfo() {
        return this.info.get(0);
    }

    public List<SolveInfo> getInfo() {
        return this.info;
    }

    public void setEngineStatus(final String engineState) {
        this.engineState = engineState;
    }

    public void setInfo(final List<SolveInfo> info) {
        this.info = info;
    }

}
