package it.unibo.alice.lpaas.ws.persistence;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import it.unibo.alice.lpaas.ws.security.Roles;

@Stateless
@LocalBean
public class UserStorage {

    @PersistenceContext(unitName = "myPU")
    EntityManager storage;

    public UserStorage() {

    }

    /**
     * If valid, and if not already present, persists the User with the credentials
     * received as parameter.
     *
     * @param username
     *            : the username of the User to persist.
     * @param password
     *            : the password of the User to persist.
     * @param role
     *            : the Role of the User to persist.
     */
    public void addUser(final String username, final String password, final Roles role) {
        if (!UserEntity.validateUsername(username)) {
            throw new IllegalArgumentException("Username is not valid");
        }
        if (!UserEntity.validatePassword(password)) {
            throw new IllegalArgumentException("Password is not valid");
        }
        if (getUser(username) != null) {
            throw new IllegalArgumentException("User already exists");
        }

        final UserEntity user = new UserEntity(username, password, role);
        this.storage.persist(user);
        this.storage.flush();
    }

    /**
     * If valid, and if not already present, persists the User received as
     * parameter.
     *
     * @param user
     *            : the User to persist.
     */
    public void addUser(final UserEntity user) {
        if (user == null) {
            throw new NullPointerException("User can't be null");
        }
        if (!UserEntity.validateUsername(user.getUsername())) {
            throw new IllegalArgumentException("Username is not valid");
        }
        if (!UserEntity.validatePassword(user.getPassword())) {
            throw new IllegalArgumentException("Password is not valid");
        }
        if (getUser(user.getUsername()) != null) {
            throw new IllegalArgumentException("User already exists");
        }

        this.storage.persist(user);
        this.storage.flush();
    }
    
    public void updateUser(final UserEntity user) {
        if (user == null) {
            throw new NullPointerException("User can't be null");
        }
        if (!UserEntity.validateUsername(user.getUsername())) {
            throw new IllegalArgumentException("Username is not valid");
        }
        if (!UserEntity.validatePassword(user.getPassword())) {
            throw new IllegalArgumentException("Password is not valid");
        }

        this.storage.createQuery("UPDATE UserEntity u SET u.password = :pwd, u.role = :role WHERE u.username = :uname")
            .setParameter("uname", user.getUsername())
            .setParameter("pwd", user.getPassword())
            .setParameter("role", user.getRole())
            .executeUpdate();
        this.storage.flush();
    }

    /**
     * Gets a List of all the Users currently persisted in the persistence context.
     *
     * @return a List<User> with all the Users currently persisted.
     */
    public List<UserEntity> getAllUsers() {
        final Query q = this.storage.createQuery("SELECT u FROM UserEntity u");
        return q.getResultList();
    }

    /**
     * Finds and returns the User associated with the given username.
     *
     * @param username
     *            : the username of the User to get.
     * @return the User entity associated with the given username or null if no User
     *         with this username exists.
     */
    public UserEntity getUser(final String username) {
        final UserEntity user = this.storage.find(UserEntity.class, username);
        System.out.println("Trovato user: " + user);
        return user;
    }

    /**
     * Gets the current Role of the User associated with the given username.
     *
     * @param username
     *            : the username of the User to check.
     * @return the Role of the User, or null if no User with the given username
     *         exists.
     */
    public Roles getUserRole(final String username) {
        final UserEntity user = getUser(username);
        if (user == null) {
            return null;
        }
        return user.getRole();
    }

    /**
     * Gets all the Users with the given Role.
     *
     * @param role
     *            : the Role of the users to look for.
     * @return a List<User> of the users with the given Role.
     */
    public List<UserEntity> getUsersWithRole(final Roles role) {
        final Query q = this.storage.createQuery("SELECT u FROM UserEntity u WHERE u.role = :userRole")
                .setParameter("userRole", role);
        return q.getResultList();
    }

    /**
     * Removes the User associated with the username from the persistence context.
     *
     * @param username
     *            : the username of the User to remove.
     */
    public void removeUser(final String username) {
        final UserEntity u = getUser(username);
        if (u == null) {
            return;
        }
        this.storage.remove(u);
        this.storage.flush();
    }

}
