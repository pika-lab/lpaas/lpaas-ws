package it.unibo.alice.lpaas.ws.security;

import java.security.PrivateKey;
import java.security.PublicKey;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.EcJwkGenerator;
import org.jose4j.jwk.EllipticCurveJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.EllipticCurves;
import org.jose4j.lang.JoseException;

import it.unibo.alice.lpaas.ws.persistence.UserEntity;
import it.unibo.alice.lpaas.ws.persistence.UserStorage;

/**
 * This component is the main access point, for all the other components of the
 * application, to request security-related operations. This component stores
 * the signing and encryption keys for the engine state and the authentication
 * token, and provides methods to simplify the operations of signature,
 * encryption, verification and decryption of the JwtClaims and engine state.
 *
 * @author Andrea Muccioli
 *
 */
@DependsOn("StartupOperations")
@Singleton
@LocalBean
public class JwtSecurityManager {

    @EJB
    private UserStorage manager;

    private EllipticCurveJsonWebKey ConfSigKey = null;
    private EllipticCurveJsonWebKey ConfEncKey = null;
    private EllipticCurveJsonWebKey EngineSigKey = null;
    private EllipticCurveJsonWebKey EngineEncKey = null;

    public JwtSecurityManager() {
    }

    /**
     * Decrypts the JWE using the provided private key.
     *
     * @param key
     *            : the private key to use.
     * @param compactSerialization
     *            : the serialization of the JWE to decrypt.
     * @return the decrypted payload of the JWE.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String decrypt(final PrivateKey key, final String compactSerialization) throws JoseException {
        final JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setCompactSerialization(compactSerialization);
        jwe.setKey(key);
        final String payload = jwe.getPlaintextString();
        return payload;
    }

    /**
     * Decrypts and verifies the engine token returning the JSON serialization of
     * the engine as String
     *
     * @param serialization
     *            : the JWE serialization of the engine.
     * @return the JSON serialization of the engine.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String decryptAndVerifyEngine(final String serialization) throws JoseException {
        final String jwePayload = decrypt(this.EngineEncKey.getPrivateKey(), serialization);
        return verify(this.EngineSigKey.getPublicKey(), jwePayload);
    }

    /**
     * Decrypts and verifies the authentication token returning the JWT claims.
     *
     * @param token
     *            : the encrypted and signed authentication token.
     * @return the JwtClaims.
     * @throws InvalidJwtException
     */
    @Lock(LockType.READ)
    public JwtClaims decryptAndVerifyToken(final String token) throws InvalidJwtException {
        final JwtConsumer consumer = new JwtConsumerBuilder().setRequireExpirationTime()
                .setAllowedClockSkewInSeconds(30).setRequireSubject().setExpectedIssuer("it.unibo.alice.tuprolog")
                .setDecryptionKey(this.ConfEncKey.getPrivateKey()).setVerificationKey(this.ConfSigKey.getPublicKey())
                .build();

        final JwtClaims jwtClaims = consumer.processToClaims(token);
        return jwtClaims;
    }

    /**
     * Encrypts the payload using the provided public key and algorithm.
     *
     * @param key
     *            : the public key to use.
     * @param algorithm
     *            : the algorithm to use.
     * @param payload
     *            : the data to encrypt.
     * @return the JWE serialization of the encrypted data.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String encrypt(final PublicKey key, final String algorithm, final String payload) throws JoseException {
        final JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setAlgorithmHeaderValue(algorithm);
        final String encAlg = ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256;
        jwe.setEncryptionMethodHeaderParameter(encAlg);
        jwe.setKey(key);
        jwe.setPayload(payload);

        return jwe.getCompactSerialization();
    }

    /**
     * Encrypts the JWS serialization of the JWT token with the public ConfEncKey
     * and returns the compact serialization of the JWE.
     *
     * @param payload
     *            : the serialization of the JWS
     * @return the String serialization of the JWE.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String encryptAndGetSerialization(final String payload) throws JoseException {
        final JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.ECDH_ES_A128KW);
        final String encAlg = ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256;
        jwe.setEncryptionMethodHeaderParameter(encAlg);
        jwe.setKey(this.ConfEncKey.getPublicKey());
        jwe.setKeyIdHeaderValue(this.ConfEncKey.getKeyId());
        jwe.setContentTypeHeaderValue("JWT");
        jwe.setPayload(payload);
        final String jweSerialization = jwe.getCompactSerialization();

        return jweSerialization;
    }

    /**
     * Creates the claims for the authentication JWT.
     *
     * @param username
     *            : the username of the user.
     * @param role
     *            : the Role of the user
     * @return the JwtClaims of the authentication JWT.
     */
    @Lock(LockType.READ)
    public JwtClaims getConfigurationClaims(final String username, final Roles role) {
        final JwtClaims claims = new JwtClaims();
        claims.setIssuer("it.unibo.alice.tuprolog");
        claims.setExpirationTimeMinutesInTheFuture(10);
        claims.setGeneratedJwtId();
        claims.setIssuedAtToNow();
        claims.setNotBeforeMinutesInThePast(2);
        claims.setSubject(username);
        claims.setClaim("role", role.name());

        return claims;
    }

    /**
     * Initializes all the keys and sets the key ids.
     *
     */
    @PostConstruct
    private void initialize() {
        try {
            this.ConfSigKey = EcJwkGenerator.generateJwk(EllipticCurves.P256);
            this.ConfSigKey.setKeyId("Configuration Signature Key");
            this.ConfEncKey = EcJwkGenerator.generateJwk(EllipticCurves.P256);
            this.ConfEncKey.setKeyId("Configuration Encription Key");
            this.EngineSigKey = EcJwkGenerator.generateJwk(EllipticCurves.P256);
            this.EngineSigKey.setKeyId("Engine Signature Key");
            this.EngineEncKey = EcJwkGenerator.generateJwk(EllipticCurves.P256);
            this.EngineEncKey.setKeyId("Engine Encription Key");
        } catch (final JoseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates new keys for the signing and encryption of the engine state.
     *
     * @throws JoseException
     */
    @Lock(LockType.WRITE)
    public void regenerateEngineKeys() throws JoseException {
        this.EngineSigKey = EcJwkGenerator.generateJwk(EllipticCurves.P256);
        this.EngineEncKey = EcJwkGenerator.generateJwk(EllipticCurves.P256);
    }

    /**
     * Signs the payload using the provided private key and algorithm.
     *
     * @param key
     *            : the private key to use.
     * @param algorithm
     *            : the algorithm to use.
     * @param payload
     *            : the data to sign.
     * @return the JWS serialization of the signed data.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String sign(final PrivateKey key, final String algorithm, final String payload) throws JoseException {
        final JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(payload);
        jws.setKey(key);
        jws.setAlgorithmHeaderValue(algorithm);
        return jws.getCompactSerialization();
    }

    /**
     * Signs and encrypts the engine JSON serialization.
     *
     * @param engineJsonState
     *            : the JSON serialization of the engine state as String.
     * @return the JWE serialization as String.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String signAndEncryptEngine(final String engineJsonState) throws JoseException {
        final String jwsSerialization = sign(this.EngineSigKey.getPrivateKey(),
                AlgorithmIdentifiers.ECDSA_USING_P256_CURVE_AND_SHA256, engineJsonState);
        final String jweSerialization = encrypt(this.EngineEncKey.getPublicKey(),
                KeyManagementAlgorithmIdentifiers.ECDH_ES_A128KW, jwsSerialization);
        return jweSerialization;
    }

    /**
     * Signs the claims using the private key of ConfSigKey and returns the compact
     * serialization
     *
     * @param claims
     *            : the claims to sign.
     * @return the String serialization of the signed claims.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String signClaimsAndGetSerialization(final JwtClaims claims) throws JoseException {
        final JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setKeyIdHeaderValue(this.ConfSigKey.getKeyId());
        jws.setKey(this.ConfSigKey.getPrivateKey());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.ECDSA_USING_P256_CURVE_AND_SHA256);
        final String serialization = jws.getCompactSerialization();
        return serialization;
    }

    /**
     * Gets the role associated with the given username and password or null if the
     * username and password aren't associated with any user.
     *
     * @param username
     *            : the username of the user
     * @param password
     *            : the password of the user
     * @return the Role associated with the credentials or null
     */
    @Lock(LockType.READ)
    public Roles validate(final String username, final String password) {
        if (!UserEntity.validateUsername(username)) {
            return null;
        }
        final UserEntity u = this.manager.getUser(username);
        if (u == null) {
            return null;
        }
        if (!password.equals(u.getPassword())) {
            return null;
        }
        return u.getRole();
    }

    /**
     * Verifies the JWS using the provided public key.
     *
     * @param key
     *            : the public key to use.
     * @param compactSerialization
     *            : the serialization of the JWS to verify.
     * @return the verified payload of the JWS.
     * @throws JoseException
     */
    @Lock(LockType.READ)
    public String verify(final PublicKey key, final String compactSerialization) throws JoseException {
        final JsonWebSignature jws = new JsonWebSignature();
        jws.setCompactSerialization(compactSerialization);
        jws.setKey(key);
        if (jws.verifySignature()) {
            return jws.getPayload();
        } else {
            throw new IllegalArgumentException("Signature is not valid");
        }
    }

}
