package it.unibo.alice.lpaas.ws.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.HttpHeaders;

import org.javatuples.Pair;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;

import it.unibo.alice.lpaas.ws.security.Roles;

/**
 * This component is an interceptor class needed to verify user credentials in a
 * transparent way in other components. Its functionalities are tightly tied to
 * the use of the annotation @RequiresAuth .
 *
 * @author Andrea Muccioli
 *
 */
public class AuthCheckInterceptor {

    private static String TOKEN_TYPE = "[\\w]+";
    private static String BASE_64_CHAR = "[a-z0-9\\-_]";
    private static Pattern AUTH_HEADER_PATTERN = Pattern.compile("(?:(?<type>" + TOKEN_TYPE + ")\\s+)?(?<token>"
            + BASE_64_CHAR + "+\\." + BASE_64_CHAR + "+\\." + BASE_64_CHAR + "+)", Pattern.CASE_INSENSITIVE);

    @EJB
    private it.unibo.alice.lpaas.ws.security.JwtSecurityManager security;

    /**
     * Interceptor method that checks the authentication token before the execution
     * of the business logic. It verifies if the user has a Role of equal or higher
     * level than the one specified in the @RequiresAuth annotation. Only methods
     * with the @RequiresAuth annotation are affected by this interceptor.
     *
     * @param ctx
     * @return returns control to the intercepted method if the verification had
     *         success, returns a FORBIDDEN (403) or UNAUTHORIZED (401) Response
     *         otherwise.
     * @throws Exception
     */
    @AroundInvoke
    public Object checkAuth(final InvocationContext ctx) throws Exception {
        final Optional<Roles> requiredRole = getRequiredRole(ctx);
        if (!requiredRole.isPresent()) {
            return ctx.proceed();
        }

        final Roles required = requiredRole.get();
        final Roles userRole;
        final Optional<String> authHeader = getAuthorizationHeaderString(ctx);
        if (authHeader.isPresent()) {
            final Matcher m = AUTH_HEADER_PATTERN.matcher(authHeader.get());
            if (!m.matches() /* || !m.group("type").equalsIgnoreCase("JWT") */) {
                throw new NotAuthorizedException("Missing JWT token or unsupported token type");
            }
            final String token;
            token = m.group("token");
            try {
                final JwtClaims jwtClaims = this.security.decryptAndVerifyToken(token);
                userRole = Roles.valueOf(jwtClaims.getClaimValue("role").toString().toUpperCase());
            } catch (final InvalidJwtException e) {
                throw new NotAuthorizedException("Bad token");
            }
        } else {
            userRole = Roles.GUEST;
        }

        if (required.compareTo(userRole) > 0) {
            throw new ForbiddenException(String.format("Role %s can't access this service", userRole));
        }

        return ctx.proceed();
    }

    private Optional<String> getAuthorizationHeaderString(final InvocationContext ctx) {
        final RequiresRole[] ann = ctx.getMethod().getAnnotationsByType(RequiresRole.class);
        if (ann.length > 0) {
            return getParametersAnnotation(ctx.getMethod()).filter(a -> a.getValue1() instanceof HeaderParam)
                    .filter(a -> HttpHeaders.AUTHORIZATION.equals(((HeaderParam) a.getValue1()).value()))
                    .map(a -> a.getValue0()).findFirst().map(i -> (String) ctx.getParameters()[i]);
        }
        return Optional.empty();
    }

    private Stream<Pair<Integer, Annotation>> getParametersAnnotation(final Method method) {
        final Annotation[][] annotations = method.getParameterAnnotations();
        return IntStream.iterate(0, i -> i + 1)
                .limit(annotations.length)
                .mapToObj(i -> i)
                .flatMap(i -> Arrays.stream(annotations[i]).map(a -> new Pair<>(i, a)));
            
    }

    private Optional<Roles> getRequiredRole(final InvocationContext ctx) {
        return Optional.ofNullable(ctx.getMethod().getAnnotation(RequiresRole.class)).map(RequiresRole::value);
    }
}
