package it.unibo.alice.lpaas.ws.persistence;

import java.util.List;
import java.util.stream.Stream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchSolutionsException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.SolutionsAlreadyExistException;

@Stateless
@LocalBean
public class SolutionsStorage {

    @PersistenceContext(unitName = "myPU")
    private EntityManager storage;

    public SolutionsStorage() {

    }

    public SolutionsEntity addSolutionSet(final SolutionsEntity ss) throws SolutionsAlreadyExistException {
        if (isHookPresent(ss.getHook())) {
            throw new SolutionsAlreadyExistException(ss);
        }

        ss.setVersion(0);
        ss.setTimestampToNow();

        this.storage.persist(ss);
        this.storage.flush();
        return ss;
    }

    public SolutionsEntity addVersion(final SolutionsEntity solution) {
        solution.setVersion(getLastVersionValue(solution.getHook()) + 1);
        solution.setTimestampToNow();
        this.storage.persist(solution);
        this.storage.flush();
        return solution;
    }

    @SuppressWarnings("unchecked")
    public Stream<SolutionsEntity> getAll() {
        return this.storage.createQuery("SELECT ss FROM SolutionsEntity ss").getResultList().stream();
    }

    @SuppressWarnings("unchecked")
    public Stream<SolutionsEntity> getAllLastVersions() {
        return this.storage.createQuery(
                "SELECT ss1 FROM SolutionsEntity ss1 WHERE ss1.version = (SELECT MAX(ss2.version) FROM SolutionsEntity ss2 WHERE ss1.name = ss2.name)")
                .getResultList().stream();
    }

    public SolutionsEntity getLastVersion(final String hook) throws NoSuchSolutionsException {
        final List<?> results = this.storage.createQuery(
                "SELECT se1 FROM SolutionsEntity se1 WHERE se1.name = :name AND se1.version = (SELECT MAX(se2.version) FROM SolutionsEntity se2 WHERE se2.name = :name)")
                .setParameter("name", hook).getResultList();
        if (results.size() > 0) {
            return (SolutionsEntity) results.get(0);
        } else {
            throw new NoSuchSolutionsException(hook);
        }

    }

    public long getLastVersionValue(final String hook) {
        try {
            return getLastVersion(hook).getVersion();
        } catch (final NoSuchSolutionsException e) {
            return 0L;
        }
    }

    public SolutionsEntity getVersion(final String hook, final long version) throws NoSuchSolutionsException {
        final List<?> results = this.storage
                .createQuery("SELECT se FROM SolutionsEntity se WHERE se.name = :name AND se.version = :version")
                .setParameter("name", hook).setParameter("version", version).getResultList();
        if (results.size() > 0) {
            return (SolutionsEntity) results.get(0);
        } else {
            throw new NoSuchSolutionsException(hook, version);
        }
    }

    public long getVersionCount(final String hook) {
        final Number result = (Number) this.storage
                .createQuery("SELECT COUNT(ss) FROM SolutionsEntity ss WHERE ss.name = :name")
                .setParameter("name", hook).getSingleResult();
        return result.longValue();
    }

    public boolean isHookPresent(final String hook) {
        return getVersionCount(hook) > 0L;
    }

    public void removeAll() {
        this.storage.createQuery("DELETE FROM SolutionsEntity ss").executeUpdate();
    }

    public void removeSolution(final String hook) throws NoSuchSolutionsException {
        final int affectedRows = this.storage.createQuery("DELETE FROM SolutionsEntity ss WHERE ss.name = :name")
                .setParameter("name", hook).executeUpdate();

        if (affectedRows <= 0) {
            throw new NoSuchSolutionsException(hook);
        }
    }

    public void removeVersion(final String hook, final long version) throws NoSuchSolutionsException {
        final int affectedRows = this.storage
                .createQuery("DELETE FROM SolutionsEntity ss WHERE ss.name = :name AND ss.version = :version")
                .setParameter("name", hook).setParameter("version", version).executeUpdate();

        if (affectedRows <= 0) {
            throw new NoSuchSolutionsException(hook, version);
        }
    }
}
