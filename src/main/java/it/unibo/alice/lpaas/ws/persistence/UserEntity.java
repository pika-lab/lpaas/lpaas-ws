package it.unibo.alice.lpaas.ws.persistence;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import it.unibo.alice.lpaas.ws.security.Roles;

/**
 * This entity specifies how the application persists the users' information.
 * The class contains the needed getters and setters to access the fields and
 * methods to check the validity of usernames and passwords matching them
 * against set regular expressions. The class can also generate a default admin
 * User with the specific method.</br>
 *
 * @author Andrea Muccioli
 *
 */
@Entity
@Table(name = "USERS")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Regular expression used to test the validity of usernames.</br>
     * </br>
     *
     * Matches strings between 5 and 15 characters that can only contain lowercase
     * characters, digits, and the symbols '_' and '-'.
     */
    private static final String USERNAME_REGEX = "^[a-z0-9_-]{5,15}$";

    /**
     * Regular expression used to test the validity of passwords.</br>
     * </br>
     *
     * (?=.*\\d) --> must contain at least one digit from 0-9</br>
     * (?=.*[a-z]) --> must contain at least one lowercase character</br>
     * (?=.*[A-Z]) --> must contain at least one uppercase character</br>
     * (?=.*[@#$%,]) --> must contain at least one symbol in the list "@#$%,"</br>
     * .{6,20} --> matches anything that satisfies the previous conditions and whose
     * length is between 6 and 20 characters
     *
     */
    private static final String PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%,]).{6,20})";

    private static final String defaultAdminUsername = "admin";
    private static final String defaultAdminPsw = "Adm1n@";

    /**
     * Creates a default admin User, using the set defaultAdminUsername and
     * defaultAdminPsw.
     *
     * @return the created default admin User.
     */
    public static UserEntity getDefaultAdmin() {
        return new UserEntity(defaultAdminUsername, defaultAdminPsw, Roles.ADMIN);
    }

    /**
     * Checks the validity of the password matching it against the set regular
     * expression.
     *
     * @param password
     *            : the password to validate
     * @return true if valid, false if not.
     */
    public static boolean validatePassword(final String password) {
        final Pattern pattern = Pattern.compile(PASSWORD_REGEX);
        final Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    /**
     * Checks the validity of the username matching it against the set regular
     * expression.
     *
     * @param username
     *            : the username to validate
     * @return true if valid, false if not.
     */
    public static boolean validateUsername(final String username) {
        final Pattern pattern = Pattern.compile(USERNAME_REGEX);
        final Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }

    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Roles role;

    public UserEntity() {
    }

    public UserEntity(final String username, final String password) {
        this(username, password, Roles.GUEST);
    }

    public UserEntity(final String username, final String password, final Roles role) {
        if (!UserEntity.validatePassword(password)) {
            throw new IllegalArgumentException("Password is not valid");
        }
        if (!UserEntity.validateUsername(username)) {
            throw new IllegalArgumentException("Username is not valid");
        }
        this.username = username;
        this.password = password;
        this.role = role;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof UserEntity)) {
            return false;
        }
        final UserEntity other = (UserEntity) o;
        if (other.getUsername().equals(this.username) && other.getPassword().equals(this.password)
                && other.getRole().equals(this.role)) {
            return true;
        }
        return false;
    }

    public String getPassword() {
        return this.password;
    }

    public Roles getRole() {
        return this.role;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(final String password) {
        if (!UserEntity.validatePassword(password)) {
            throw new IllegalArgumentException("Password is not valid");
        }
        this.password = password;
    }

    public void setRole(final Roles role) {
        this.role = role;
    }

    public void setUsername(final String username) {
        if (!UserEntity.validateUsername(username)) {
            throw new IllegalArgumentException("Username is not valid");
        }
        this.username = username;
    }

    @Override
    public String toString() {
        return "Username: " + this.username + " ; Password: " + this.password + " ; Role: " + this.role.name() + " ;";
    }

}