package it.unibo.alice.lpaas.ws.security;

/**
 * Enum that represents a Role and therefore an access level, in the server
 * application.
 *
 * @author Andrea Muccioli
 *
 */
public enum Roles {
    GUEST, SENSOR, CONFIGURATOR, ADMIN;

    /**
     * Gets the role, if any, right above the one that invoked the method.
     *
     * @return the role right above the current one, or null if this is already the
     *         highest role.
     */
    public Roles getRoleAbove() {
        final int newOrdinal = this.ordinal() + 1;
        if (newOrdinal >= Roles.values().length) {
            return null;
        }
        return Roles.values()[newOrdinal];
    }

    /**
     * Gets the role, if any, right below the one that invoked the method.
     *
     * @return the role right below the current one, or null if this is already the
     *         lowest role.
     */
    public Roles getRoleBelow() {
        final int newOrdinal = this.ordinal() - 1;
        if (newOrdinal < 0) {
            return null;
        }
        return Roles.values()[newOrdinal];
    }
}
