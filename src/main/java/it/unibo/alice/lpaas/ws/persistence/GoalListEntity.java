package it.unibo.alice.lpaas.ws.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import alice.tuprolog.Term;
import it.unibo.alice.lpaas.ws.utils.Terms;

@Entity
@Table(name = "GOAL_LISTS")
public class GoalListEntity implements Serializable {

    private static final long serialVersionUID = -5066820215440166120L;

    @Transient
    private transient Term termView;

    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "goal_list", nullable = false)
    private String goalList;

    public GoalListEntity() {
    }

    public GoalListEntity(final String name, final String goalList) {
        this.name = Objects.requireNonNull(name);
        this.goalList = Optional.of(goalList).orElse("");
    }

    public void appendSubGoal(final String term) {
        appendSubGoalTerm(Term.createTerm(term));
    }

    public void appendSubGoalTerm(final Term term) {
        final List<Term> goalList = Terms.flattenConjunction(getGoalListTerm());
        goalList.add(term);
        setGoalListTerm(Terms.conjunctList(goalList));
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof GoalListEntity)) {
            return false;
        }
        final GoalListEntity other = (GoalListEntity) obj;
        if (this.goalList == null) {
            if (other.goalList != null) {
                return false;
            }
        } else if (!this.goalList.equals(other.goalList)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public String getGoalList() {
        return this.goalList;
    }

    public Term getGoalListTerm() {
        if (!isCachePresent()) {
            regenerateCache();
        }
        return this.termView;
    }

    public int getLength() {
        return Terms.flattenConjunction(getGoalListTerm()).size();
    }

    public String getName() {
        return this.name;
    }

    public Term getSubGoal(final int index) {
        final List<Term> goalList = Terms.flattenConjunction(getGoalListTerm());
        return goalList.get(index);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.goalList == null ? 0 : this.goalList.hashCode());
        result = prime * result + (this.name == null ? 0 : this.name.hashCode());
        return result;
    }

    private void invalidateCache() {
        this.termView = null;
    }

    private boolean isCachePresent() {
        return this.termView != null;
    }

    private void regenerateCache() {
        this.termView = Term.createTerm(this.goalList);
    }

    public void setGoalList(final String goalList) {
        this.goalList = goalList;
        invalidateCache();
    }

    public void setGoalListTerm(final String term) {
        this.setGoalListTerm(Term.createTerm(term));
    }

    public void setGoalListTerm(final Term term) {
        this.termView = term;
        syncCache();
    }

    public void setName(final String name) {
        this.name = name;
    }

    private void syncCache() {
        this.goalList = Optional.ofNullable(this.termView)
                .map(Terms::flattenConjunction)
                .map(list -> list.stream().map(Term::toString).collect(Collectors.joining(", ")))
                .orElse("");
                
    }

    @Override
    public String toString() {
        return "GoalList [name=" + this.name + ", goalList=" + this.goalList + "]";
    }

}