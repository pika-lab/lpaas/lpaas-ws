package it.unibo.alice.lpaas.ws.persistence;

import java.io.Serializable;

public class NamedAndVersionedPrimaryKey implements Serializable {

    private static final long serialVersionUID = 5041898886618839125L;

    private String name;
    private Long version;

    public NamedAndVersionedPrimaryKey() {

    }

    public NamedAndVersionedPrimaryKey(final String name, final Long version) {
        this.name = name;
        this.version = version;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof NamedAndVersionedPrimaryKey)) {
            return false;
        }
        final NamedAndVersionedPrimaryKey other = (NamedAndVersionedPrimaryKey) obj;
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!this.version.equals(other.version)) {
            return false;
        }
        return true;
    }

    public String getName() {
        return this.name;
    }

    public long getVersion() {
        return this.version.longValue();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.name == null ? 0 : this.name.hashCode());
        result = prime * result + (this.version == null ? 0 : this.version.hashCode());
        return result;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setVersion(final long version) {
        this.version = version;
    }

}
