package it.unibo.alice.lpaas.ws.persistence;

import java.util.List;
import java.util.stream.Stream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchATheoryException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.TheoryAlreadyExistsException;

@Stateless
@LocalBean
public class TheoryStorage {

    @PersistenceContext(unitName = "myPU")
    private EntityManager storage;

    public TheoryStorage() {

    }

    public TheoryEntity addTheory(final String name, final String theory) throws TheoryAlreadyExistsException {
        return addTheory(new TheoryEntity(name, theory));
    }

    public TheoryEntity addTheory(final TheoryEntity theory) throws TheoryAlreadyExistsException {
        if (isTheoryPresent(theory.getName())) {
            throw new TheoryAlreadyExistsException(theory);
        }
        theory.setVersion(0);
        theory.setTimestampToNow();
        this.storage.persist(theory);
        this.storage.flush();
        return theory;
    }

    public TheoryEntity addVersion(final String name, final String theory) {
        return addVersion(new TheoryEntity(name, theory));
    }

    public TheoryEntity addVersion(final TheoryEntity pt) {
        pt.setVersion(getLastVersionValue(pt.getName()) + 1);
        pt.setTimestampToNow();
        this.storage.persist(pt);
        this.storage.flush();
        return pt;
    }

    @SuppressWarnings("unchecked")
    public Stream<TheoryEntity> getAll() {
        return this.storage.createQuery("SELECT pt FROM TheoryEntity pt").getResultList().stream();
    }

    @SuppressWarnings("unchecked")
    public Stream<TheoryEntity> getAllLastVersions() {
        return this.storage.createQuery(
                "SELECT pt1 FROM TheoryEntity pt1 WHERE pt1.version = (SELECT MAX(pt2.version) FROM TheoryEntity pt2 WHERE pt1.name = pt2.name)")
                .getResultList().stream();
    }

    public TheoryEntity getLastVersion(final String name) throws NoSuchATheoryException {
        final List<?> results = this.storage.createQuery(
                "SELECT pt1 FROM TheoryEntity pt1 WHERE pt1.name = :name AND pt1.version = (SELECT MAX(pt2.version) FROM TheoryEntity pt2 WHERE pt2.name = :name)")
                .setParameter("name", name).getResultList();
        if (results.size() > 0) {
            return (TheoryEntity) results.get(0);
        } else {
            throw new NoSuchATheoryException(name);
        }
    }

    public long getLastVersionValue(final String name) {
        try {
            return getLastVersion(name).getVersion();
        } catch (final NoSuchATheoryException e) {
            return 0L;
        }
    }

    public TheoryEntity getVersion(final String name, final long version) throws NoSuchATheoryException {
        final List<?> results = this.storage
                .createQuery("SELECT pt FROM TheoryEntity pt WHERE pt.name = :name AND pt.version = :version")
                .setParameter("name", name).setParameter("version", version).getResultList();
        if (results.size() > 0) {
            return (TheoryEntity) results.get(0);
        } else {
            throw new NoSuchATheoryException(name, version);
        }
    }

    public long getVersionCount(final String name) {
        final Number result = (Number) this.storage
                .createQuery("SELECT COUNT(pt) FROM TheoryEntity pt WHERE pt.name = :name").setParameter("name", name)
                .getSingleResult();
        return result.longValue();
    }

    public boolean isTheoryPresent(final String name) {
        return getVersionCount(name) > 0L;
    }

    public void removeTheory(final String name) throws NoSuchATheoryException {
        final int affectedRows = this.storage.createQuery("DELETE FROM TheoryEntity pt WHERE pt.name = :name")
                .setParameter("name", name).executeUpdate();

        if (affectedRows <= 0) {
            throw new NoSuchATheoryException(name);
        }
    }

    public void removeVersion(final String name, final long version) throws NoSuchATheoryException {
        final int affectedRows = this.storage
                .createQuery("DELETE FROM TheoryEntity pt WHERE pt.name = :name AND pt.version = :version")
                .setParameter("name", name).setParameter("version", version).executeUpdate();

        if (affectedRows <= 0) {
            throw new NoSuchATheoryException(name, version);
        }
    }
}
