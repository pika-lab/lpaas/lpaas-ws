package it.unibo.alice.lpaas.ws.persistence.exceptions;

import it.unibo.alice.lpaas.ws.persistence.SolutionsEntity;

public class NoSuchSolutionsException extends Exception {

    private static final long serialVersionUID = 7996230983328837859L;

    public NoSuchSolutionsException(final SolutionsEntity solutionSet) {
        this(solutionSet.getHook());
    }

    public NoSuchSolutionsException(final String hook) {
        super(String.format("No such a solution entity with hook: '%s'", hook));
    }

    public NoSuchSolutionsException(final String hook, final long version) {
        super(String.format("No such a solution entity with hook '%s' and version '%d'", hook, version));
    }
}
