package it.unibo.alice.lpaas.ws.persistence;

import java.util.List;
import java.util.stream.Stream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import it.unibo.alice.lpaas.ws.persistence.exceptions.GoalListAlreadyExistsException;
import it.unibo.alice.lpaas.ws.persistence.exceptions.NoSuchAGoalListException;

@Stateless
@LocalBean
public class GoalListStorage {

    @PersistenceContext(unitName = "myPU")
    private EntityManager storage;

    public GoalListStorage() {

    }

    public GoalListEntity addGoalList(final GoalListEntity goalList) throws GoalListAlreadyExistsException {
        if (isGoalListPresent(goalList.getName())) {
            throw new GoalListAlreadyExistsException(goalList);
        }
        this.storage.persist(goalList);
        this.storage.flush();
        return goalList;
    }

    public GoalListEntity addGoalList(final String name, final String goals) throws GoalListAlreadyExistsException {
        return addGoalList(new GoalListEntity(name, goals));
    }

    public GoalListEntity appendSubGoal(final String name, final String goal) throws NoSuchAGoalListException {
        final GoalListEntity GoalListEntity = getGoalList(name);

        GoalListEntity.appendSubGoal(goal);

        return updateGoalList(GoalListEntity);
    }

    @SuppressWarnings("unchecked")
    public Stream<GoalListEntity> getAll() {
        return this.storage.createQuery("SELECT gl FROM GoalListEntity gl").getResultList().stream();
    }

    public GoalListEntity getGoalList(final String name) throws NoSuchAGoalListException {
        final List<?> results = this.storage.createQuery("SELECT gl FROM GoalListEntity gl WHERE gl.name = :name")
                .setParameter("name", name).getResultList();
        if (results.size() > 0) {
            return (GoalListEntity) results.get(0);
        } else {
            throw new NoSuchAGoalListException(name);
        }
    }

    public boolean isGoalListPresent(final String name) {
        try {
            return getGoalList(name) != null;
        } catch (final NoSuchAGoalListException e) {
            return false;
        }
    }

    public void removeGoalList(final String name) throws NoSuchAGoalListException {
        final int affectedRows = this.storage.createQuery("DELETE FROM GoalListEntity gl WHERE gl.name = :name")
                .setParameter("name", name).executeUpdate();

        if (affectedRows <= 0) {
            throw new NoSuchAGoalListException(name);
        }
    }

    public GoalListEntity updateGoalList(final GoalListEntity pt) {
        this.storage.merge(pt);
        return pt;
    }
}
