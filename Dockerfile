FROM java:openjdk-8-alpine

COPY ./ /lpaas-ws/

WORKDIR /lpaas-ws/

RUN ./gradlew build

ENV GRD_OPTS --console=plain --no-daemon

EXPOSE 8080

CMD ./gradlew run $GRD_OPTS