# Logic Programming as a Service

_LPaaS_ is an an effective enabling technology for intelligent IoT. 
It is a logic-based, service-oriented approach for distributed situated intelligence, conceived and designed as the natural evolution of 
LP in nowadays pervasive computing systems. 
Its purpose is to enable situated reasoning via explicit definition of the spatial-temporal structure of the environment where situated entities act and 
interact.  

_LPaaS_ in tuProlog consists in the LPaaS implementation built on top of the tuProlog system, that makes it possible to build distributed intelligent IoT environment distributing reasoning and 
inference capabilities amongst the components they have, and let them balance the computational requirements to best suit the deployment scenario at hand, for instance, embedding LPaaS in more powerful components and letting ask their services by need.

The repository contains prototype implementations built on top of the tuProlog system, which provides the required interoperability and customisation. 

The _LPaaS_ RESTful API can be found [here](https://app.swaggerhub.com/apis/dasLab-Unibo/LPaaS/1) (notice that it may not have been totally implemented).

## How to (quickly) run the LPaaS service

0. Ensure your JDK__8__ is properly installed and __configured__
	- i.e., `java -version` returns `1.8.<whatever>`
	- i.e., `javac -version` returns `1.8.<whatever>`
	- i.e., your `JAVA_HOME` environment variable is correcly set
	
0. Open a shell into the project main directory (the one containing the `buil.gradle` file) and run `.\gradlew run`
	- on Posix systems (Linux and MacOS) the command to be run is `./gradlew run`
	- you may need to add execution permission to the file by running `chmod u+x gradlew` 

0. Wait until the following output appears, denoting a correct initialization:
    ```
	Payara Micro URLs:
	http://localhost:8080/lpaas
	
	'lpaas' REST Endpoints:
	GET     /lpaas/application.wadl
	POST    /lpaas/auth
	GET     /lpaas/goals
	POST    /lpaas/goals
	DELETE  /lpaas/goals/{name}
	GET     /lpaas/goals/{name}
	POST    /lpaas/goals/{name}
	PUT     /lpaas/goals/{name}
	GET     /lpaas/goals/{name}/{index}
	DELETE  /lpaas/solutions
	GET     /lpaas/solutions
	POST    /lpaas/solutions
	DELETE  /lpaas/solutions/{hook}
	GET     /lpaas/solutions/{hook}
	DELETE  /lpaas/solutions/{hook}/history/{version}
	GET     /lpaas/solutions/{hook}/history/{version}
	GET     /lpaas/theories
	POST    /lpaas/theories
	DELETE  /lpaas/theories/{name}
	GET     /lpaas/theories/{name}
	POST    /lpaas/theories/{name}
	POST    /lpaas/theories/{name}/facts
	GET     /lpaas/theories/{name}/facts/{functor}
	PUT     /lpaas/theories/{name}/facts/{functor}
	GET     /lpaas/theories/{name}/facts/{functor}/history/{version}
	GET     /lpaas/theories/{name}/history/{version}
	```

0. You can now interact with the LPaaS service by means of any HTTP client
	- I suggest a web browser or [Postman](https://www.getpostman.com/) for testing/debugging purposes
	
## Developing LPaaS

Developers are welcome since many features are still missing!

To develop the LPaaS service, you need the following things:

- Eclipse for __JavaEE__ developers
    + https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/keplersr2
    
- Glassfish or Payara __Full__
	+ https://javaee.github.io/glassfish/
	+ https://www.payara.fish/downloads
	
- A proper configuration, as described [here](https://bitbucket.org/tuProlog/lpaas-tuprolog/downloads/InstallationGuide-LPaaS-as-a-RESTfulWS.pdf)

- A running database: open a shell into the Glassfish/Payara `/bin` folder and run `asadmin start-database`
	+ You may have to do so every time you restart your computer


## Issue tracking (for students too!)

Feel free to open an issue on this repository whenever you cannot succeed in running the service, importing it in Eclipse, a feature is missing, or the service is not behaving as desired!

